package neu.smitchandarana.connecteddevices.labs.module08;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.smitchandarana.connecteddevices.labs.module06.MqttClientConnector;

public class TempActuatorSubscriberApp {

	/**
	 * Receive message using MQTT protocol (subscribes to Mqtt Topic) 
	 * 
	 * @var subscriberApp: instance variable for TempActuatorSubscriberApp
	 * @var mqttClient: instance variable for MqqtClientConnector
	 * @var host: Broker address for Mqtt
	 * @var pemfileName: file location of the Ubidots pem file
	 * @var UBIDOTS_VARIABLE_LABEL: string constant for ubidots variable name
	 * @var UBIDOTS_VARIABLE_LABEL: String constant for ubidots Device name
	 * @var UBIDOTS_TOPIC_DEFAULT: Topic name to subscribe/publish
	 */
	private static TempActuatorSubscriberApp subscriberApp;
	private MqttClientConnector mqttClient;
	private String host = ConfigConst.DEFAULT_UBIDOTS_SERVER;
	private String pemFileName = "C:\\\\Users\\\\smit2\\\\git\\\\connected-devices-java" + ConfigConst.UBIDOTS
			+ ConfigConst.CERT_FILE_EXT;
	private String authToken = "BBFF-mI0lxKfgSCtv6feODWluRGLx2G0wVW";
	public static final String UBIDOTS_VARIABLE_LABEL = "/tempactuator";
	public static final String UBIDOTS_DEVICE_LABEL = "/controller";
	public static final String UBIDOTS_TOPIC_DEFAULT = "/v1.6/devices" + UBIDOTS_DEVICE_LABEL + UBIDOTS_VARIABLE_LABEL;

	/**
	 * MqttSubClientTestApp constructor
	 */
	public TempActuatorSubscriberApp() {
		super();
	}

	/**
	 * method to connect mqqt client to broker and publish the message
	 * @param topicName: name of the MQTT session topic in string
	 * 
	 */
	public void start(String topicName) {
		try {
			mqttClient = new MqttClientConnector(host, authToken, pemFileName);
			mqttClient.connect();
			while (true) {
				mqttClient.subscribeToTopic(topicName);
				Thread.sleep(60000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mqttClient.disconnect();
		}
	}

	/**
	 * Main function of TempActuatorSubscriber class
	 */
	public static void main(String[] args) {

		subscriberApp = new TempActuatorSubscriberApp();
		try {
			subscriberApp.start(UBIDOTS_TOPIC_DEFAULT);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
