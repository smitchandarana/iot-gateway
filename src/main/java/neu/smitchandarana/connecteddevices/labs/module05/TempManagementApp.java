package neu.smitchandarana.connecteddevices.labs.module05;

import neu.smitchandarana.connecteddevices.labs.common.DataUtil;
import neu.smitchandarana.connecteddevices.labs.common.SensorData;


/**
 * Creating a class TempManagement App that instantiates the DataUtil object
 * inside the demo function and reads the json formatted data from the path 
 * and displays the sensor readable data in the console output
 */

public class TempManagementApp {

	/**
	 * demo() method is created for instantiating DataUtil object and for
	 * displaying the output
	 */
	
	public static void demo()
	{
		DataUtil sensor = new DataUtil();
		SensorData sen = sensor.JsonToSensorData(null, "C:\\Users\\smit2\\git\\iot-device\\apps\\labs\\module05\\sensordata.txt");
		System.out.println(sen);
	}

}