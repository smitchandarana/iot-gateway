package neu.smitchandarana.connecteddevices.labs.module07;

import java.util.logging.Logger;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import com.labbenchstudios.edu.connecteddevices.common.DataUtil;
import com.labbenchstudios.edu.connecteddevices.common.SensorData;

public class TempResourceHandler extends CoapResource {
	/*
	 * @var logger 
	 * @var sensorData --> instance variable for SensorData class
	 * @var DataUtil --> instance variable for DataUtil class
	 */
	private static final Logger logger = Logger.getLogger(TempResourceHandler.class.getName());
	private SensorData sensorData = new SensorData();
	private DataUtil dataUtil = new DataUtil();

	/**
	 *default constructor 
	 */
	public TempResourceHandler() {
		super("temperature", true);
	}

	/* 
	 * responds to GET Request 
	 */
	@Override
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.VALID, "GET worked!");
		logger.info("Received GET request from client.");
	}

	/* 
	 * responds to POST request
	 */
	@Override
	public void handlePOST(CoapExchange ce) {
		ce.respond(ResponseCode.CREATED,"POST worked !");
		logger.info("Received POST request from client.");
		String recievedJson = new String(ce.getRequestPayload());
		logger.info("Recieved \n" + recievedJson);
		sensorData = dataUtil.JsonToSensorData(recievedJson);
		logger.info("\n" + sensorData.toString());
		logger.info("POST successfull \n");
	}
	
	/*
	 * responds to PUT request 
	 */
	@Override
	public void handlePUT(CoapExchange ce) {
		ce.respond(ResponseCode.CHANGED,"PUT worked!");
		String recievedjson = new String(ce.getRequestPayload());
		logger.info("update: \n" + recievedjson);
		sensorData = dataUtil.JsonToSensorData(recievedjson);
		logger.info("\n" + sensorData.toString());
		logger.info("PUT Request was successful.\n");
	}

	/* 
	 * responds to DELETE request
	 */
	@Override
	public void handleDELETE(CoapExchange ce) {
		ce.respond(ResponseCode.DELETED,"DELETE worked!");
		logger.info("Received DELETE  request from client.");
	}
}
