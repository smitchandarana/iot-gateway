package neu.smitchandarana.connecteddevices.labs.module07;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.smitchandarana.connecteddevices.labs.common.DataUtil;
import neu.smitchandarana.connecteddevices.labs.common.SensorData;

public class CoapClientConnector {
	/*
	 * @var protocol --> the protocol used
	 * 
	 * @var host --> ip/domain of the host
	 * 
	 * @var serverAddr --> the complete address of the server class
	 * 
	 * @var clientConn --> instance variable for Coap client class
	 * 
	 * @var sensorData --> instance variable for SensorData class
	 * 
	 * @var dataUtil --> instance variable for DataUtil class
	 */
	private static final Logger logger = Logger.getLogger(CoapClientConnector.class.getName());
	private String protocol;
	private String host;
	private String serverAddr;
	private CoapClient clientConn;
	private SensorData sensorData;
	private DataUtil dataUtil;

	// constructors
	/**
	 * Default.
	 *
	 */
	public CoapClientConnector() {
		this(ConfigConst.DEFAULT_COAP_SERVER);
	}

	/**
	 * Constructor.
	 *
	 * @param host
	 */
	public CoapClientConnector(String host) {
		super();
		this.protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		} else {
			this.host = ConfigConst.DEFAULT_COAP_SERVER;
		}
		this.serverAddr = this.protocol + "://" + this.host;
		logger.info("Using URL for server conn: " + this.serverAddr);
		sensorData = new SensorData();
		dataUtil = new DataUtil();
	}

	/**
	 * initialize the CoAP client
	 * 
	 * @param resourceName
	 */
	private void initClient(String resourceName) {

		if (clientConn != null) {
			clientConn.shutdown();
			clientConn = null;
		}
		try {
			if (resourceName != null) {
				serverAddr += "/" + resourceName;
			}
			clientConn = new CoapClient(serverAddr);
			logger.info("Created client connection to server / resource: " + serverAddr);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to connect to broker: " + getCurrentUri(), e);
		}
	}

	/**
	 * run different request commands to server
	 * 
	 * @param resourceName
	 */
	public void runTests(String resourceName) {
		try {
			initClient(resourceName);
			logger.info("Current URI: " + getCurrentUri());
			sensorData.addValue(4.00f);
			pingServer();
			sendGetRequest();
			sendPostRequest(dataUtil.SensorDataToJson(sensorData));
			sendGetRequest();
			sensorData.addValue(13.00f);
			sendPutRequest("name:SenseHAT Sensor timeStamp : 2019-11-02 19:38:04.427970, curValue: 23.5, avgValue : 23.78, minValue: 19.12, maxValue: 30.24, currentPressure: 0");
			sendGetRequest();
			sendDeleteRequest();
			pingServer();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to issue request to CoAP server.", e);
		}
	}

	/**
	 * Returns the CoAP client URI (if set, otherwise returns the serverAddr, or
	 * null).
	 *
	 * @return String
	 */
	public String getCurrentUri() {
		return (this.clientConn != null ? this.clientConn.getURI() : this.serverAddr);
	}

	/**
	 * ping the server
	 */
	public void pingServer() {
		logger.info("Sending ping...");
		clientConn.ping();
	}

	/**
	 * send DELETE request
	 */
	public void sendDeleteRequest() {
		logger.info("Sending Delete request...");
		CoapResponse response = null;
		response = clientConn.delete();
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * send GET request
	 */
	public void sendGetRequest() {
		logger.info("Sending Get request...");
		CoapResponse response = null;
		response = clientConn.get();
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * send POST request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPostRequest(String payload) {
		logger.info("Sending Post request...");
		CoapResponse response = null;
		logger.info("Sensor Data: " + payload);
		response = clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}

	/**
	 * send PUT request
	 * 
	 * @param payload --> the message to be sent as POST request
	 */
	public void sendPutRequest(String payload) {
		logger.info("Sending Put request...");
		CoapResponse response = null;
		logger.info("Sensor Data: " + payload);
		response = clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received.");
		}
	}
}
