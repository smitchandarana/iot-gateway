package neu.smitchandarana.connecteddevices.labs.module07;

public class CoapServerTestApp {
	/*
	 * @App --> instance variable for CoapServerTestApp
	 * @coapServerConnector --> instance variable for CoapServerConnector class
	 */
	private static CoapServerTestApp App;
	private CoapServerConnector coapServer;

	/**
	 * default constructor
	 */
	public CoapServerTestApp() {
		super();
	}

	/**
	 * starts the coapServer 
	 */
	public void start() {
		coapServer = new CoapServerConnector();
		coapServer.start();
	}

	/**
	 * Main method
	 */
	public static void main(String[] args) {
		App = new CoapServerTestApp();
		try {
			App.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
