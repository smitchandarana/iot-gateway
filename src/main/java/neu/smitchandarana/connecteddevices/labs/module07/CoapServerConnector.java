package neu.smitchandarana.connecteddevices.labs.module07;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.logging.Logger;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

public class CoapServerConnector {
	/*
	 * @var logger
	 * @var coapServer --> instance variable for CoapServer class
	 * @var port --> port number to connect
	 */
	private static final Logger logger = Logger.getLogger(CoapServerConnector.class.getName());
	private CoapServer coapServer;
	private int port;

	// constructors
	/**
	 * Default.
	 *
	 */
	public CoapServerConnector() {
		super();
		this.port = ConfigConst.DEFAULT_COAP_PORT;
	}

	/**
	 *initialize and Start the CoapServer and add resource handler 
	 */
	public void start() {
		if (coapServer == null) {
			logger.info("Creating CoAP server instance and 'temp' handler...");
			coapServer = new CoapServer();
			TempResourceHandler tempHandler = new TempResourceHandler();
			addEndpoints();
			coapServer.add(tempHandler);
		}
		logger.info("Starting CoAP server...");
		coapServer.start();
	}

	/**
	 *Stop the CoAP server 
	 */
	public void stop() {
		logger.info("Stopping CoAP server...");
		coapServer.stop();
	}

	/**
	 * add endpoint to the coapserver 
	 */
	private void addEndpoints() {
		for (InetAddress iAddr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
			if (iAddr.isLoopbackAddress()) {
				InetSocketAddress bind2Addr = new InetSocketAddress(iAddr, port);
				coapServer.addEndpoint(new CoapEndpoint(bind2Addr));
			}
		}

	}
}
