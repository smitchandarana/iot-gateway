package neu.smitchandarana.connecteddevices.labs.module01;
import Polling;

//Using PollingManager to get the the System info 

public class SystemPerformanceAdaptor {
	{


		super();
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		_pollManager = new DevicePollingManager(2);
	}
	public void startPolling()
	{
		_Logger.info("Creating and scheduling CPU Utilization poller...");
		_pollManager.schedulePollingTask(
				new SystemCpuUtilTask("CPU Utilization", _pollCycle), _pollCycle);
		_Logger.info("Creating and scheduling Memory Utilization poller...");
		_pollManager.schedulePollingTask(
				new SystemMemUtilTask("Memory Utilization", _pollCycle), _pollCycle);
	}
}


