package neu.smitchandarana.connecteddevices.labs.module02;


public void connect()
{
	if (! _isConnected) {
		_Logger.info("Initializing SMTP gateway...");
		Properties props = new Properties();
		String portStr = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY);
		props.put(ConfigConst.SMTP_PROP_HOST_KEY, ConfigConst.HOST_KEY);
		props.put(ConfigConst.SMTP_PROP_PORT_KEY, portStr);
		props.put(ConfigConst.SMTP_PROP_AUTH_KEY, ConfigConst.ENABLE_AUTH_KEY);
		props.put(ConfigConst.SMTP_PROP_ENABLE_TLS_KEY, ConfigConst.ENABLE_CRYPT_KEY);
		_Logger.info(props.toString());
		Authenticator authenticator = new SmtpAuthenticator();
		_smtpSession = Session.getInstance(props, authenticator);
		_isConnected = true;
	} else {
		_Logger.info("SMTP gateway connection already initialized.");
	}
}
@Override
public boolean publishMessage(ServiceResourceInfo resource, int qosLevel, byte[] payload)
{
	if (! _isConnected) {
		connect();
	}
	boolean success = false;
	String fromAddrStr =
			ConfigUtil.getInstance().getProperty(
					ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
	try {
		Message smtpMsg = new MimeMessage(_smtpSession);
		InternetAddress fromAddr = new InternetAddress(fromAddrStr);
		InternetAddress[] toAddr =
				
				InternetAddress.parse(
						ConfigUtil.getInstance().getProperty(
								ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY));
		smtpMsg.setFrom(fromAddr);
		smtpMsg.setRecipients(Message.RecipientType.TO, toAddr);
		smtpMsg.setSubject(resource.getTopic());
		smtpMsg.setText(new String(payload));
		Transport.send(smtpMsg);
		success = true;
	} catch (Exception e) {
		_Logger.log(Level.WARNING, "Failed to send SMTP message from address: " + fromAddrStr, e);
	}
	return success;
}

