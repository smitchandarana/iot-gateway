package neu.smitchandarana.connecteddevices.labs.module02;
import com.labbenchstudios.edu.connecteddevices.common.ServiceResourceInfo;

import labs.common.SensorData;
public class TempSensorEmulator {
	public void sendNotification()
	{
		try {
			SensorData sensorData = super.getSensorData();
			ServiceResourceInfo resourceInfo =
					new ServiceResourceInfo(
							sensorData.getName(),
							"Temperature above threshold",
							sensorData.getName() + "Threshold Crossing",
							String.valueOf(sensorData.getErrorCode()));
			// my publishMessage() API takes byte[] as the payload for flexibility
			_smtpConnector.publishMessage(
					resourceInfo,
					1,
					sensorData.getBytes());
		} catch (Exception e) {
			_Logger.log(Level.WARNING, "Failed to send SMTP message.", e);
		}
	}


}
