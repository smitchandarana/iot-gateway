package com.labbenchstudios.edu.connecteddevices.common;

import com.google.gson.Gson;

public class DataUtil {
	/**
	 * class having methods to convert sensor data to json
	 * and convert json back to sensor data 
	 */
	
	public String SensorDataToJson(SensorData sensordata)
	{
		/**
		 * @param: jsonSd --> String to store the json data
		 * @param: gson --> Gson object
		 * @param: sensordata: Sensor data passed as method argument 
		 * @Return: string --> json having sensor data
		 */
		String jsonSd;
		Gson gson = new Gson();
		jsonSd = gson.toJson(sensordata);
		return jsonSd;
	}
	
	public SensorData JsonToSensorData(String jsondata)
	{
		/**
		 * @param: sensorData --> SensorData object to store the sensor data from json data
		 * @param: gson --> Gson object
		 * @param: jsondata --> json data passed as method argument
		 * @return: SensorData --> sensor data converted from json data
		 */
		SensorData sensorData;
		Gson gson = new Gson();
		sensorData = gson.fromJson(jsondata, SensorData.class);
		return sensorData;
	}	
}
